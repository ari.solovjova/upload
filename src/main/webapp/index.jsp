<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>File upload</title>
<meta charset="UTF-8">
</head>
<body>
    <div>
        <div>
            <h1>
                <%
                String msg = (String) request.getAttribute("message");
                if (msg != null) {
                    out.println(msg);
                }
                %>
            </h1>
            <p>Click on the "Choose File" button to upload a file:</p>
            <form action="Handler" method="post"
                enctype="multipart/form-data">
                <input type="file" id="myFile" name="filename" /> <br />
                <input type="submit" value="Upload" />
            </form>
        </div>
        <div>
            <form action="FileDownload" method="get">
                <h1>
                    <%
                    out.println("Download your compressed files here:");
                    %>
                </h1>
                <div>
                    <%
                    String[] compressedFiles = (String[]) request.getAttribute("compressedFiles");
                    if (compressedFiles != null) {
                        for (String file : compressedFiles) {
                            out.println("<a download href='static?download=" + file + "'>");
                            out.println(file);
                            out.println("</a>");
                            out.println("<br/>");
                        }
                    }
                    %>
                </div>
            </form>
        </div>
    </div>
</body>
</html>