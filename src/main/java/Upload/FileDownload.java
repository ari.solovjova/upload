package Upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * HttpServlet extended class for file downloading in web
 */
@WebServlet(name = "FileDownload", urlPatterns = { "/static" })
public class FileDownload extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /*
     *  Method to handle GET method request.
     *  Process compressed files for downloading
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fileName = request.getParameter("download");
        File fileToDownload = new File("src/main/resources/compressed/" + fileName);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String gurupath = "src/main/resources/compressed/";
        response.setContentType("APPLICATION/OCTET-STREAM");

        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileToDownload.getName() + "\"");

        FileInputStream fileInputStream = new FileInputStream(gurupath + fileName);
        int i;
        while ((i = fileInputStream.read()) != -1) {
            out.write(i);
        }
        fileInputStream.close();
        out.close();
    }
}
