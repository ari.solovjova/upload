package Upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

/*
 * HttpServlet extended class for file processing in web
 */
@WebServlet(name = "Handler", urlPatterns = { "/" })
public class Handler extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /*
     * 
     */
    public Handler() {
        super();
    }

    /*
     * 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            PrintWriter writer = response.getWriter();
            writer.println("Request does not contain upload data");
            writer.flush();
            return;
        } else {
            // Configures upload settings
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

            ServletFileUpload upload = new ServletFileUpload(factory);

            // Constructs the directory path to store upload file
            String uploadPath = "src/main/resources/compressed";
            // Creates the directory if it does not exist
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            try {
                // Parses the request's content to extract file data
                List<FileItem> formItems = upload.parseRequest(new ServletRequestContext(request));
                Iterator<FileItem> iter = formItems.iterator();

                // Iterates over form's fields
                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();
                    // Processes only fields that are not form fields
                    if (!item.isFormField()) {
                        String fileName = new File(item.getName()).getName();
                        String filePath = uploadPath + File.separator + fileName;
                        File storeFile = new File(filePath);

                        // Compress and save the compressed file on disk
                        item.write(storeFile);
                        Compress.zipFile(storeFile);

//                      Remove iterator for safe file deletion
                        iter.remove();

//                       Delete saved source file 
                        storeFile.delete();

                    }
                }
                request.setAttribute("message", "Upload has been done successfully!");
            } catch (Exception ex) {
                request.setAttribute("message", "There was an error: " + ex.getMessage());
            }
            
            String[] compressedFiles = uploadDir.list();
            request.setAttribute("compressedFiles", compressedFiles);
            
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }

}
